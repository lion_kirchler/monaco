import { Component, OnInit, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Range as MonacoRange } from 'monaco-editor';
import { ViewEncapsulation } from '@angular/core';

import { MonacoAutocompleteService } from '../monaco-autocomplete.service';

export interface ReadonlyLines {
	start: number,
	end: number
}
//NOTE: usually and
export interface MonacoOptions {
	editor: {
		theme: "vs-light" | "vs-dark",
		language: string,
	},
	readonlyLines: ReadonlyLines;
}

@Component({
  selector: 'app-monaco',
  templateUrl: './monaco.component.html',
  styleUrls: ['./monaco.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
	provide: NG_VALUE_ACCESSOR,
	useExisting: forwardRef(() => MonacoComponent),
	multi: true
  }]
})
export class MonacoComponent implements OnInit, ControlValueAccessor {
	@Input()
	public options: MonacoOptions = {
		editor: {
			theme: "vs-light",
			language: "javascript",
		},
		readonlyLines: { start: 0, end: 0 }
	};

	@Output()
	public codeNoReadonly = new EventEmitter<string>();

	public codeInitial = '';
	public code = '';
	
	private deltaDecorations = [];

	public constructor() {}

	public ngOnInit(): void {}

	public onMonacoInit(monaco: any) {
		//console.log((window as any).monaco);

		this.codeInitial = this.code;
		this.handleCodeNoReadonly();
		this.readonlyDecorators(monaco);

		//prevent writing readonly
		monaco.onDidChangeModelContent((e: any) => {
			//console.log(monaco);
			if(this.isReadonlyActive()) {
				const posData = this.getEditableRange(false);
				const posDataInitial = this.getEditableRange(true);
				const readonlyUnchanged = this.code.substring(posData.end + 1, this.code.length) === this.codeInitial.substring(posDataInitial.end + 1, this.codeInitial.length) &&
					this.code.substring(0, posData.start) === this.codeInitial.substring(0, posDataInitial.start);
				if (!readonlyUnchanged) {
					monaco.getModel().undo();
					return;
				}
				this.readonlyDecorators(monaco);
			}
			this.handleCodeNoReadonly();
		});
	}

	private readonlyDecorators(monaco: any) {
		//visualize readonly
		let decorations = [];
		if(this.options.readonlyLines.start > 0) {
			decorations.push({
				range: new MonacoRange(1, 1, this.options.readonlyLines.start, 1),
				options: {
					isWholeLine: true,
					inlineClassName: 'monacoReadOnlyLine'
				}
			})
		}
		if(this.options.readonlyLines.end > 0) {
			decorations.push({
				range: new MonacoRange(monaco.getModel().getLineCount() - this.options.readonlyLines.end + 1, 1, monaco.getModel().getLineCount(), 1),
				options: {
					isWholeLine: true,
					inlineClassName: 'monacoReadOnlyLine'
				}
			});
		}
		this.deltaDecorations = monaco.deltaDecorations(this.deltaDecorations, decorations);
	}

	private handleCodeNoReadonly() {
		let codeNoReadonly;

		if (this.isReadonlyActive()) {
			const posData = this.getEditableRange(false);
			codeNoReadonly = this.code.substring(posData.start, posData.end);
		} else {
			codeNoReadonly = this.code;
		}

		this.codeNoReadonly.emit(codeNoReadonly);
	}

	private getEditableRange(initialCode: boolean): {start: number, end: number} {
		const code = initialCode ? this.codeInitial : this.code;
		let posFirst = 0;
		for(let i = 0; i < this.options.readonlyLines.start && (posFirst = code.indexOf("\n", posFirst + 1)) != -1;i++) {}
		let posLast = code.length;
		for(let i = 0; i < this.options.readonlyLines.end && (posLast = code.lastIndexOf("\n", posLast - 1)) != -1;i++) {}
		return {start: posFirst + 1, end: posLast};
	}

	private isReadonlyActive() {
		return this.options.readonlyLines.start > 0 && this.options.readonlyLines.end > 0;
	}

    writeValue(value: any) {
		if(typeof value === "string") this.code = value;
    }

    registerOnChange(fn: any) {
        this.onChange = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouched = fn;
	}

    private onChange = (value: any) => {}
    private onTouched = () => { };
}
