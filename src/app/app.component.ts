import { Component } from '@angular/core';
import { MonacoOptions } from './monaco/monaco.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	public monacoJSOptions: MonacoOptions = {
		editor: {
			theme: "vs-light",
			language: "javascript",
		},
		readonlyLines: { start: 2, end: 1 }
	};
	public codeJS =
`import * as moment from "moment";
function task(data) {
    console.log("Hello World");
    return data;
}`
	public codeJSNoReadonly = '';

	public monacoSQLOptions: MonacoOptions = {
		editor: {
			theme: "vs-light",
			language: "sql" //does not exist,
		},
		readonlyLines: { start: 0, end: 0 }
	};
	public codeSQL = 'SELECT * FROM users;';
	public codeSQLNoReadonly = '';

	onCodeJSNoReadonly(code: string) {
		this.codeJSNoReadonly = code;
	}

	onCodeSQLNoReadonly(code: string) {
		this.codeSQLNoReadonly = code;
	}
}
