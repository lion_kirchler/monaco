import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxMonacoEditorConfig } from 'ngx-monaco-editor';

@Injectable({
  providedIn: 'root'
})
export class MonacoAutocompleteService {
	private folder = "assets/monaco-libs";
	private libs = [
		"moment.d.ts"
	];

	constructor(private httpClient: HttpClient, @Inject("monacoConfig") monacoConfig: NgxMonacoEditorConfig) {
		console.log(monacoConfig);
		monacoConfig.onMonacoLoad = () => this.onMonacoLoad();
	}

	private onMonacoLoad() {	
		let monaco = (window as any).monaco;
		for(let lib of this.libs) {
			this.httpClient.get(`${this.folder}/${lib}`, {responseType: 'text'}).subscribe({
				next: data => {
					monaco.languages.typescript.javascriptDefaults.addExtraLib(data, `inmemory://model/${lib}`);
				}
			});
		}
	}
}
