
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MonacoComponent } from './monaco/monaco.component';
import { MonacoAutocompleteService } from './monaco-autocomplete.service';

const monacoConfig: NgxMonacoEditorConfig = {
  onMonacoLoad: () => { throw new Error("onMonacoLoad not set by MonacoAutocompleteService"); }
};

@NgModule({
  declarations: [
    AppComponent,
    MonacoComponent
  ],
  imports: [
    BrowserModule,
	MonacoEditorModule.forRoot(monacoConfig),
	FormsModule,
	HttpClientModule
  ],
  providers: [
	  MonacoAutocompleteService,
	  { provide: "monacoConfig", useValue: monacoConfig },
	  {
		provide: APP_INITIALIZER,
		useFactory: (_monacoAutocompleteService: MonacoAutocompleteService) => () => Promise.resolve(null),
    	deps: [MonacoAutocompleteService],
    	multi: true
	  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
